﻿//////////////////////////////////////////////////////////////////////////
// CLASSE: Minesweeper
// Classe pricipal, contiens la logique du jeu
// Auteur: Harold Lamarre
// Automne 2015
/////////////////////////////////////////////////////////////////////////

package  {
	import flash.display.*;
	import flash.events.*;
	public class Minesweeper extends Sprite{

		
		/*
		easy 9x9 10 mines
		intermediate 16x16 40 mines
		expert 30x16 99
		custom 9x9 à 30x24 10 à 668 mines
		*/
		
		static private var _caseWidthCount:uint;//combien de cases en largeur et hauteur
		static private var _caseHeightCount:uint;
		private var _minesCount:uint;
		private var _caseWidth:uint;
		
		private var _conteneur:Sprite=new Sprite();
		private var _contour:Sprite=new Sprite();
		static private var _face:MovieClip=new Face();
		static private var _temps:Temps=new Temps();
		private var _conteurM:MineConteur=new MineConteur();
		static private var _tableCases:Array=[];
		static private var _listeMines:Array=[];
		private var _lastCaseHighlight:Case;
		private var _bufferClicks:Array=[];
		private var _minesDejaPlacer:Boolean=false;
		static private var _isEnded:Boolean=false;
		private var _numbersCount:uint=0;
		private var _numbersFound:uint=0;
		
		public function Minesweeper(cWidth:uint=30,cHeight:uint=16,cMine:uint=99) {
			_caseWidthCount=cWidth;
			_caseHeightCount=cHeight;
			_minesCount=cMine
			addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(e:Event):void{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			//logique
			addChild(_conteneur);
			_caseWidth=Case.getWidth();
			for(var i=0; i<_caseWidthCount; i++){//création des cases
				var tempRanger:Array=[];
				for(var j=0; j<_caseHeightCount; j++){
					var caseTemp:Case=new Case(i,j);
					tempRanger.push(caseTemp);
					_conteneur.addChild(caseTemp);
				}
				_tableCases.push(tempRanger);
			}
			
			//visuel
			_conteneur.x=(stage.stageWidth-_conteneur.width)/2;
			_conteneur.y=(stage.stageHeight-_conteneur.height)/2;
			
			addChild(_face);
			_face.stop();
			_face.y=_conteneur.y-_face.height-14;
			_face.x=(stage.stageWidth-_face.width)/2;
			
			addChild(_temps);
			_temps.x=_conteneur.x+_conteneur.width-_temps.width-6;
			_temps.y=_conteneur.y-15-_temps.height;
			
			addChild(_conteurM);
			_conteurM.setTotal(_minesCount);
			_conteurM.x=_conteneur.x+4;
			_conteurM.y=_conteneur.y-15-_conteurM.height;
			
			//draw contour dynamique
			_contour.graphics.beginFill(0x808080);//top pis gauche
			_contour.graphics.moveTo(_conteneur.x-3,_conteneur.y+_conteneur.height+3)
			_contour.graphics.lineTo(_conteneur.x-3,_conteneur.y-3);
			_contour.graphics.lineTo(_conteneur.x+_conteneur.width+3,_conteneur.y-3);
			_contour.graphics.lineTo(_conteneur.x+_conteneur.width,_conteneur.y);
			_contour.graphics.lineTo(_conteneur.x,_conteneur.y);
			_contour.graphics.lineTo(_conteneur.x,_conteneur.y+_conteneur.height)
			_contour.graphics.endFill()
			
			_contour.graphics.beginFill(0xffffff);//bas pis droite
			_contour.graphics.moveTo(_conteneur.x-3,_conteneur.y+_conteneur.height+3)
			_contour.graphics.lineTo(_conteneur.x+_conteneur.width+3,_conteneur.y+_conteneur.height+3);
			_contour.graphics.lineTo(_conteneur.x+_conteneur.width+3,_conteneur.y-3);
			_contour.graphics.lineTo(_conteneur.x+_conteneur.width,_conteneur.y);
			_contour.graphics.lineTo(_conteneur.x+_conteneur.width,_conteneur.y+_conteneur.height);
			_contour.graphics.lineTo(_conteneur.x,_conteneur.y+_conteneur.height)
			_contour.graphics.endFill()
			
			
			_contour.graphics.beginFill(0x808080);//top pis gauche
			_contour.graphics.moveTo(_conteurM.x-7,_conteurM.y+_conteurM.height+6)
			_contour.graphics.lineTo(_conteurM.x-7,_conteurM.y-6);
			_contour.graphics.lineTo(_temps.x+_temps.width+9,_temps.y-6);
			_contour.graphics.lineTo(_temps.x+_temps.width+7,_temps.y-4);
			_contour.graphics.lineTo(_conteurM.x-5,_conteurM.y-4);
			_contour.graphics.lineTo(_conteurM.x-5,_conteurM.y+_conteurM.height+4)
			_contour.graphics.endFill()
			
			_contour.graphics.beginFill(0xffffff);//bas pis droite
			_contour.graphics.moveTo(_conteurM.x-7,_conteurM.y+_conteurM.height+6)
			_contour.graphics.lineTo(_temps.x+_temps.width+9,_temps.y+_temps.height+6);
			_contour.graphics.lineTo(_temps.x+_temps.width+9,_temps.y-6);
			_contour.graphics.lineTo(_temps.x+_temps.width+7,_temps.y-4);
			_contour.graphics.lineTo(_temps.x+_temps.width+7,_temps.y+_temps.height+4);
			_contour.graphics.lineTo(_conteurM.x-5,_conteurM.y+_conteurM.height+4)
			_contour.graphics.endFill()
			
			
			addChild(_contour);
			//fin draw contour
			
			stage.addEventListener(MouseEvent.MOUSE_DOWN,startLoopDown);
			stage.addEventListener(MouseEvent.MOUSE_UP,stopLoopDown);
			_face.addEventListener(MouseEvent.CLICK,reset);
			_face.addEventListener(MouseEvent.MOUSE_DOWN,faceDown);
			_face.addEventListener(MouseEvent.MOUSE_OUT,faceOut);
			_face.addEventListener(MouseEvent.MOUSE_OVER,faceIn);
			stage.addEventListener(MouseEvent.RIGHT_MOUSE_DOWN,flag);
			trace("it's a start")
		}
		
		
		
		//events
		private function startLoopDown(e:MouseEvent):void{//mouse down
			if(_isEnded)return;
			addEventListener(Event.ENTER_FRAME,loopDown);
		}
		private function stopLoopDown(e:MouseEvent):void{//click du joueur
			_face.isClicked=false;
			if(e.target is Case){
				if(_isEnded)return;
				if(!_minesDejaPlacer){//placement des mines au premier click pour ne jamais pouvoir click sur une mine en premier
					_minesDejaPlacer=true;
					_temps.start();
					if(_minesCount<_caseWidthCount*_caseHeightCount){//placement des mines
						var minesPlacer:uint=0;
						while(minesPlacer<_minesCount){
							var cible:Case=_tableCases[M.rand(0,_caseWidthCount-1)][M.rand(0,_caseHeightCount-1)];
							if(_lastCaseHighlight!=cible){//la case cible n'est pas celle clicker
								if(cible.setMine()){//si la case réeussit à etre tranformé en mine
									_listeMines.push(cible);
									minesPlacer++;
								}
							}
						}
					}else{
						trace("Trop de mines")//si la quantité de mines est plus grande que la quantité de case
					}
					
					for(var i:uint=0;i<_listeMines.length;i++){//indique le nombre de mine environantes
						var posXCible:uint=_listeMines[i].getPos()[0];//get pos
						var posYCible:uint=_listeMines[i].getPos()[1];
						//augmente en try catch pour ne pas avoir d'erreur si la mine est au bord de la grille
						try{_tableCases[posXCible-1][posYCible-1].upOne();}catch(e:TypeError){};
						try{_tableCases[posXCible][posYCible-1].upOne()}catch(e:TypeError){};
						try{_tableCases[posXCible+1][posYCible-1].upOne()}catch(e:TypeError){};
						try{_tableCases[posXCible-1][posYCible].upOne()}catch(e:TypeError){};
						try{_tableCases[posXCible+1][posYCible].upOne()}catch(e:TypeError){};
						try{_tableCases[posXCible-1][posYCible+1].upOne()}catch(e:TypeError){};
						try{_tableCases[posXCible][posYCible+1].upOne()}catch(e:TypeError){};
						try{_tableCases[posXCible+1][posYCible+1].upOne()}catch(e:TypeError){};
					}
					
					for(i=0;i<_tableCases.length;i++){
						for(var j:uint=0;j<_tableCases[i].length;j++){
							if(_tableCases[i][j].isItNumber()){
								_numbersCount++
							}
						}
					}				
				}
				//fin creation
				_bufferClicks.push(_lastCaseHighlight);
				while(_bufferClicks.length!=0){//flood check
					try{
						if(_bufferClicks[0].click()){//si la case est vide, click les autres autours
							posXCible=_bufferClicks[0].getPos()[0];//get pos
							posYCible=_bufferClicks[0].getPos()[1];
							
							try{_bufferClicks.push(_tableCases[posXCible-1][posYCible-1])}catch(e:TypeError){};//check autour de pos
							try{_bufferClicks.push(_tableCases[posXCible][posYCible-1])}catch(e:TypeError){};
							try{_bufferClicks.push(_tableCases[posXCible+1][posYCible-1])}catch(e:TypeError){};
							try{_bufferClicks.push(_tableCases[posXCible-1][posYCible])}catch(e:TypeError){};
							try{_bufferClicks.push(_tableCases[posXCible+1][posYCible])}catch(e:TypeError){};
							try{_bufferClicks.push(_tableCases[posXCible-1][posYCible+1])}catch(e:TypeError){};
							try{_bufferClicks.push(_tableCases[posXCible][posYCible+1])}catch(e:TypeError){};
							try{_bufferClicks.push(_tableCases[posXCible+1][posYCible+1])}catch(e:TypeError){};
						}
					}catch(e:TypeError){}
					_bufferClicks.shift();
				}
				_numbersFound=0;
				for(i=0;i<_tableCases.length;i++){
					for(j=0;j<_tableCases[i].length;j++){
						if(_tableCases[i][j].isItNumber()&&_tableCases[i][j].isItShown()){
							_numbersFound++
						}
					}
				}
				if(_numbersFound==_numbersCount){
					win()
				}
				
				
			}
			removeEventListener(Event.ENTER_FRAME,loopDown);
		}
		
		private function loopDown(e:Event):void{//affiche le visuel enfoncé
			
			try{
				_lastCaseHighlight.goUp();
			}catch(e:TypeError){
				
			}
			try{
				//trace(_lastCaseHighlight);
				
				var caseTemp:Case=getAtMouse();
				_lastCaseHighlight=caseTemp;
				_lastCaseHighlight.goDown();
			}catch(e:TypeError){
				//trace("mouse out of bound")
			}
		}
		//events stop
		//private
		private function getAtMouse():Case{
			return _tableCases[Math.floor(_conteneur.mouseX/_caseWidth)][Math.floor(_conteneur.mouseY/_caseWidth)]
		}
		
		
		private function reset(e:MouseEvent):void{
			_face.isClicked=false;
			_face.gotoAndStop(1);
			trace("reset")
			_listeMines=[];
			_isEnded=false;
			_minesDejaPlacer=false;
			_numbersCount=0;
			_conteurM.reset();
			for(var i:uint=0;i<_caseWidthCount;i++){//a optimiser
				for(var j:uint=0;j<_caseHeightCount;j++){
					_tableCases[i][j].reset();
				}
			}
			_temps.reset();
		}
		
		//controle visuel de la face
		private function faceDown(e:MouseEvent):void{
			_face.gotoAndStop(2);
			_face.isClicked=true;
		}
		private function faceOut(e:MouseEvent):void{
			if(_face.isClicked){
				if(!_isEnded){
					_face.gotoAndStop(1);
				}else{
					_face.gotoAndStop(4)
				}
			}
		}
		private function faceIn(e:MouseEvent):void{
			if(_face.isClicked){
				_face.gotoAndStop(2);
			}
		}
		//fin controle face
		
		private function flag(e:MouseEvent):void{
			trace("nope");
			try{
				if(!_isEnded){
					var state:uint=Case(e.target).toggleFlag();
					if(state==1){//si devient flag, drop le conteur de un
						_conteurM.down()
					}else if(state==2){//si devient ?
						_conteurM.up()
					}
				}
			}catch(e:TypeError){}
		}
		
		private function win():void{
			_isEnded=true;
			_face.gotoAndStop(3);
			_temps.stop();
			_conteurM.endGame();
			for(var i:uint=0;i<_listeMines.length;i++){//affiche tout les mines qui n'ont pas encore été flag car la condition de victoire est d'afficher tout les cases qui ne sont pas des mines
				Case(_listeMines[i]).flagMine();
			}
		}
		//private stop
		
		public static function deadEnd():void{
			_isEnded=true;
			_face.gotoAndStop(4);
			_temps.stop();
			/*for(var i:uint=0;i<_listeMines.length;i++){//plus rapide, fonctionne pour afficher tous les mines, mais ne montre pas les flag mal placé
				trace(i)
				Case(_listeMines[i]).showMine();
			}*/
			for(var i:uint=0;i<_caseWidthCount;i++){//moins rapide, mais fidèle à l'original
				for(var j:uint=0;j<_caseHeightCount;j++){
					_tableCases[i][j].showMine();
				}
			}
		}
		
		public function selfDestroy():void{
			//destruction du visuel
			for(var i:uint=0;i<_caseWidthCount;i++){//destructions des cases
				for(var j:uint=0;j<_caseHeightCount;j++){
					_tableCases[i][j].remove();
				}
			}
			_tableCases=[];
			_listeMines=[];
			removeChild(_face);
			removeChild(_temps);
			removeChild(_conteurM);
			removeChild(_contour);
			removeChild(_conteneur);
			//destructions des events
			stage.removeEventListener(MouseEvent.MOUSE_DOWN,startLoopDown);
			stage.removeEventListener(MouseEvent.MOUSE_UP,stopLoopDown);
			_face.removeEventListener(MouseEvent.CLICK,reset);
			_face.removeEventListener(MouseEvent.MOUSE_DOWN,faceDown);
			_face.removeEventListener(MouseEvent.MOUSE_OUT,faceOut);
			_face.removeEventListener(MouseEvent.MOUSE_OVER,faceIn);
			stage.removeEventListener(MouseEvent.RIGHT_MOUSE_DOWN,flag);
			
		}
		
		
	}	
}
