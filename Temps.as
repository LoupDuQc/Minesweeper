﻿//////////////////////////////////////////////////////////////////////////
// CLASSE: Temps
// Extension de Conteur, est un Chronomètre simple avec un stop(), un start() et un reset()
// Auteur: Harold Lamarre
// Automne 2015
/////////////////////////////////////////////////////////////////////////

package  {
	import flash.events.*;

	public class Temps extends Conteur {
		private var _startTime:Number;
		private var _endTime:Number;
		private var _isRunning:Boolean=false;
		public function Temps() {
			
		}
		
		private function tick(e:Event){
			changeNumbers(Math.floor((new Date().time-_startTime)/1000))//affichage du temps sans etre affecté par les fps
			//trace((new Date().time-_startTime)/1000,new Date().time,_startTime)
		}
		
		public function reset():void{
			changeNumbers(0);
			stop();
		}
		
		public override function stop():void{
			_endTime=new Date().time;
			_isRunning=false;
			removeEventListener(Event.ENTER_FRAME,tick);
		}
		
		public function start():void{
			if(!_isRunning){
				trace("test")
				addEventListener(Event.ENTER_FRAME,tick);
				_startTime=new Date().time;
				_isRunning=true;
			}
		}
	}
}
