﻿//////////////////////////////////////////////////////////////////////////
// CLASSE: Jeu
// Contenant qui contrôle les changements de taille de la grille et le nombre de mine
// Auteur: Harold Lamarre
// Automne 2015
/////////////////////////////////////////////////////////////////////////

package  {
	import flash.display.*;
	import flash.events.*;
	import flash.ui.Keyboard;
	public class Jeu extends MovieClip{

		private var _main:Minesweeper = new Minesweeper();
		
		public function Jeu() {
			addChild(_main);
			addEventListener(Event.ADDED_TO_STAGE, init);//activation du konami code
		}
		
		//trace([Keyboard.UP, Keyboard.UP,  Keyboard.DOWN, Keyboard.DOWN, Keyboard.LEFT, Keyboard.RIGHT,  Keyboard.LEFT, Keyboard.RIGHT, 66, 65].toString())
		private var konamiCode:Array;

		 
		private function init(e:Event):void{
			removeEventListener(Event.ADDED_TO_STAGE,  init);
			// create an array to hold the input  history;
			konamiCode = new Array();
			// set  the array length to 10, since we are comparing it against a code of 10  key presses
			konamiCode.length = 10;
			stage.addEventListener(KeyboardEvent.KEY_DOWN, checkKonamiCode)
		}
		 
		private function  checkKonamiCode(e:KeyboardEvent):void{//fonction trouvé en ligne pour le code konami pour tester facilement le changement de taille de la grille avant l'ajout d'un menu
			 
			// Do  a little array work to make comparison easy.
			konamiCode.shift(); // remove first item in array;
			konamiCode.push(e.keyCode);    // add latest key press to end of  keypress history
			if (konamiCode.toString() == [Keyboard.UP, Keyboard.UP,  Keyboard.DOWN, Keyboard.DOWN, Keyboard.LEFT, Keyboard.RIGHT,  Keyboard.LEFT, Keyboard.RIGHT, 66, 65].toString()) {
				// Add you magical Konami Code Madness here.
				trace("Konami Entered!!!");
				_main.selfDestroy();//vide les tableaux et détruit les events listeners
				removeChild(_main);
				_main=new Minesweeper(9,9,10);//création de la nouvelle grille, les dimensions doivent être transféré vers des variables
				addChild(_main)
			}
		 
		}

	}
	
}
