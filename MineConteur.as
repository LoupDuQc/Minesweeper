﻿//////////////////////////////////////////////////////////////////////////
// CLASSE: MineConteur
// Extension de Conteur, fait l'interface entre Minesweeper et le compteur de mine
// Auteur: Harold Lamarre
// Automne 2015
/////////////////////////////////////////////////////////////////////////

package  {
	
	public class MineConteur extends Conteur{
		private var _total:uint;//total de mines en jeu
		private var _cur:uint;//nombre de case avec un flag
		
		public function MineConteur() {
			// constructor code
		}
		
		public function setTotal(n:uint){
			_total=n;
			_cur=n
			changeNumbers(n);
		}
		
		public function up():void{
			changeNumbers(++_cur);
		}
		
		public function down():void{
			changeNumbers(--_cur);
		}
		
		public function reset():void{
			_cur=_total;
			changeNumbers(_cur);
		}
		
		public function endGame():void{
			changeNumbers(0);
		}
		
	}
	
}
