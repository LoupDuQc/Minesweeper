﻿//////////////////////////////////////////////////////////////////////////
// CLASSE: Conteur
// Classe pour séparer le changement de l'affichage et la logique pour savoir quoi afficher
// Auteur: Harold Lamarre
// Automne 2015
/////////////////////////////////////////////////////////////////////////

package  {
	import flash.display.MovieClip;
	public class Conteur extends MovieClip{

		public function Conteur() {
			
		}
		
		protected function changeNumbers(n:int):void{
			var s:String = n.toString();
			if(n<10 && n>=0){//normalise le string a 3 chiffres
				s="00"+s;
			}else if(n<100 && n>=0){
				s="0"+s;
			}else if(n>=1000){
				s="999";
			}else if(n<0 && n>-10){
				s="-0"+ s.substr(1);
			}else if(n<=-10 && n>-100){
				//est déjà comme il faut
			}else if(n<=-100){
				s="-"+s.substring(2);//écrase le premier caractère comme dans l'original
			}else{//si ne sais pas quoi faire, ne devrais pas arriver
				s="888";
			}//fin normalisation
			//change l'affichage
			if(s.charAt(0)=="-"){_chiffre1.gotoAndStop(11)}
			else{_chiffre1.gotoAndStop(parseInt(s.charAt(0))+1);}
			_chiffre2.gotoAndStop(parseInt(s.charAt(1))+1);
			_chiffre3.gotoAndStop(parseInt(s.charAt(2))+1);
		}
	}
}
