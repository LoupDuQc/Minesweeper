﻿//////////////////////////////////////////////////////////////////////////
// CLASSE: Case
// Contiens les actions que les cases doivent faire
// Auteur: Harold Lamarre
// Automne 2015
/////////////////////////////////////////////////////////////////////////

package  {
	import flash.display.MovieClip
	public class Case extends MovieClip{
		static protected const CaseWidth:uint=16;//largeur et hauteur de tous les cases
		private var _posX:uint;
		private var _posY:uint;
		private var _type:int;//combien il y a de mine autour, 0= flood check, 1 à 8 normal, -1 = mine
		private var _flagState:uint=0;//0=normal, 1=flag, 2=?
		private var _isShown:Boolean=false;
		
		static public function getWidth():uint{//retourne la largeur d'une tuille, utiliser pour le visuel dynamique
			return CaseWidth;
		}
		
		public function Case(tempPosX:uint,tempPosY) {
			_posX=tempPosX;
			_posY=tempPosY;
			x=_posX*CaseWidth;
			y=_posY*CaseWidth;
			stop();
		}
		
		public function click():Boolean{//affiche la case aproprié
			if(!_isShown && _flagState!=1){//si n'est pas déjà affiché et n'a pas de flag
				_isShown=true;
				if(_type==-1){//si mine
					gotoAndStop(11);
					Minesweeper.deadEnd();
				}else{//si un nombre ou vide
					gotoAndStop(_type+2);
					if(_type==0){
						return true//doit afficher les tuilles environantes
					}
				}
			}
			return false//ne doit pas afficher les tuille environante
		}
		
		public function showMine(){//affichage de tous les mines pour quand le joueur click sur une première mine
			if(!_isShown && _flagState==0 && _type==-1){//si mine non trouvé
				_isShown=true;
				gotoAndStop(12);
			}else if(!_isShown && _flagState==1 && _type!=-1){//si flag mal placé
				_isShown=true;
				gotoAndStop(16);
			}
		}
		
		public function flagMine(){//affichage de tous les mines pour quand le joueur gagne si certaines mines ne sont pas encore flag
			gotoAndStop(13);
		}
		
		public function goDown():void{//visuel quand mouse_down
			if(!_isShown && _flagState==0){//si n'a pas de ?
				gotoAndStop(2);
			}else if (!_isShown && _flagState==2){//si a un ?
				gotoAndStop(15);
			}
		}
		public function goUp():void{//retour visuel normal quand le curseur sort de la case
			if(!_isShown && _flagState==0){//si n'a pas de ?
				gotoAndStop(1);
			}else if (!_isShown && _flagState==2){//si a un ?
				gotoAndStop(14);
			}
		}
		
		public function setMine():Boolean{//devient une mine
			if(_type!=-1){
				_type=-1;
				return true
			}else{
				return false
			}
		}
		
		public function isItMine():Boolean{//demande si c'est une mine
			if(_type==-1){
				return true
			}
			return false
		}
		
		public function isItNumber():Boolean{//demande si c'est un nombre
			if(_type>0){//si un nombre
				return true
			}
			return false//si mine ou vide
		}
		
		public function isItShown():Boolean{//demande si c'est déjà affiché
			if(_isShown){//si déjà affiché
				return true
			}
			return false//si mine ou vide
		}
		
		public function toggleFlag():uint{//return 1= flag, 2= ?, 3= reset
			if(!_isShown){
				switch(_flagState){
					case 0 : 
						_flagState=1;
						gotoAndStop(13);
						return 1;
					break;
					
					case 1 : 
						_flagState=2;
						gotoAndStop(14);
						return 2
					break;
					
					case 2 : 
						_flagState=0;
						gotoAndStop(1);
					break;
				}
			}
			return 3
		}
		
		public function upOne():void{//monte le conteur a affiché
			if(_type!=-1){
				_type++
			}
		}
		
		public function getPos():Array{
			return [_posX,_posY];
		}
		
		public function reset():void{
			_type = 0;
			gotoAndStop(1);
			_isShown=false;
			_flagState=0;
		}
		
		public function remove(){
			parent.removeChild(this);
		}
		
		override public function toString():String{
			return "case à : "+ _posX+ ","+_posY;
		}

	}
	
}
